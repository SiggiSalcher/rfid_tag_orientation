package mobile.sisa.anglemeasurement;

import java.util.ArrayList;

public class LookUpTable {

    private int iAngle;
    private double iRssiValue;
    private boolean bPhasePositive;
    private ArrayList<LookUpObjects> lookUpTable;
    private double prevRssi;

    public LookUpTable() {
        if (this.lookUpTable==null) {
            this.lookUpTable=new ArrayList<LookUpObjects>();
        }
        setValues();
    }

    public int iAngleOut(double iRssiValueIn, boolean sPhaseIn) {
        this.iRssiValue=iRssiValueIn;
        this.bPhasePositive = sPhaseIn;

        return iFindAngle();
    }

    private int iFindAngle() {
        for(LookUpObjects actualObject:this.lookUpTable)
        {
            //Punktlandung
            if((actualObject.getPhasePositive()&&this.bPhasePositive)||(!actualObject.getPhasePositive()&&!this.bPhasePositive))
            {
                if(actualObject.getRssi() == this.iRssiValue){
                    return actualObject.getAngle();
                }
                else if((actualObject.getRssi() +0.5) > this.iRssiValue) {
                    if(actualObject.getRssi() -0.5 < this.iRssiValue) {
                        return actualObject.getAngle();
                    }
            }
            }
                    /*(actualObject.getRssi() == this.iRssiValue
                            || (actualObject.getRssi()+0.5 >= this.iRssiValue
                            && actualObject.getRssi()-0.5 <= this.iRssiValue))) {
                this.prevRssi=actualObject.getRssi();
                return  actualObject.getAngle();
            }*/
        }
        return -1;
    }

    private void setValues() {
        //default values for lookuptable
        this.lookUpTable.add(new LookUpObjects(0,-62.73,true));
        this.lookUpTable.add(new LookUpObjects(1,-62.62	,true));
        this.lookUpTable.add(new LookUpObjects(2,-62.51	,true));
        this.lookUpTable.add(new LookUpObjects(3,-62.41	,true));
        this.lookUpTable.add(new LookUpObjects(4,-62.31	,true));
        this.lookUpTable.add(new LookUpObjects(5,-62.23	,true));
        this.lookUpTable.add(new LookUpObjects(6,-62.14	,true));
        this.lookUpTable.add(new LookUpObjects(7,-62.07	,true));
        this.lookUpTable.add(new LookUpObjects(8,-62.00	,true));
        this.lookUpTable.add(new LookUpObjects(9,-61.94	,true));
        this.lookUpTable.add(new LookUpObjects(10,-61.89,true));
        this.lookUpTable.add(new LookUpObjects(11,-61.84	,true));
        this.lookUpTable.add(new LookUpObjects(12,-61.80	,true));
        this.lookUpTable.add(new LookUpObjects(13,-61.77	,true));
        this.lookUpTable.add(new LookUpObjects(14,-61.74	,true));
        this.lookUpTable.add(new LookUpObjects(15,-61.73	,true));
        this.lookUpTable.add(new LookUpObjects(16,-61.71	,true));
        this.lookUpTable.add(new LookUpObjects(17,-61.71	,true));
        this.lookUpTable.add(new LookUpObjects(18,-61.71	,true));
        this.lookUpTable.add(new LookUpObjects(19,-61.72	,true));
        this.lookUpTable.add(new LookUpObjects(20,-61.73	,true));
        this.lookUpTable.add(new LookUpObjects(21,-61.75	,true));
        this.lookUpTable.add(new LookUpObjects(22,-61.78	,true));
        this.lookUpTable.add(new LookUpObjects( 23,-61.81	,true));
        this.lookUpTable.add(new LookUpObjects(24,-61.86	,true));
        this.lookUpTable.add(new LookUpObjects(25,-61.91	,true));
        this.lookUpTable.add(new LookUpObjects(26,-61.96	,true));
        this.lookUpTable.add(new LookUpObjects(27,-62.02	,true));
        this.lookUpTable.add(new LookUpObjects(28,-62.09	,true));
        this.lookUpTable.add(new LookUpObjects(29,-62.17	,true));
        this.lookUpTable.add(new LookUpObjects(30,-62.25	,true));
        this.lookUpTable.add(new LookUpObjects(31,-62.34	,true));
        this.lookUpTable.add(new LookUpObjects(32,-62.44	,true));
        this.lookUpTable.add(new LookUpObjects(33,-62.54	,true));
        this.lookUpTable.add(new LookUpObjects(34,-62.65	,true));
        this.lookUpTable.add(new LookUpObjects(35,-62.77	,true));
        this.lookUpTable.add(new LookUpObjects(36,-62.89	,true));
        this.lookUpTable.add(new LookUpObjects(37,-63.02	,true));
        this.lookUpTable.add(new LookUpObjects(38,-63.16	,true));
        this.lookUpTable.add(new LookUpObjects(39,-63.30	,true));
        this.lookUpTable.add(new LookUpObjects(40,-63.45	,true));
        this.lookUpTable.add(new LookUpObjects(41,-63.61	,true));
        this.lookUpTable.add(new LookUpObjects(42,-63.77	,true));
        this.lookUpTable.add(new LookUpObjects(43,-63.94	,true));
        this.lookUpTable.add(new LookUpObjects(44,	-64.12	,true));
        this.lookUpTable.add(new LookUpObjects(45,-64.31	,true));
        this.lookUpTable.add(new LookUpObjects(46,-64.50	,true));
        this.lookUpTable.add(new LookUpObjects(47,-64.69	,true));
        this.lookUpTable.add(new LookUpObjects(48,-64.90	,true));
        this.lookUpTable.add(new LookUpObjects(49,-65.11	,true));
        this.lookUpTable.add(new LookUpObjects(50,-65.33	,true));
        this.lookUpTable.add(new LookUpObjects(51,-65.56	,true));
        this.lookUpTable.add(new LookUpObjects(52,-65.79	,true));
        this.lookUpTable.add(new LookUpObjects(53,-66.03	,true));
        this.lookUpTable.add(new LookUpObjects(54,-66.27	,true));
        this.lookUpTable.add(new LookUpObjects(55,-66.53	,true));
        this.lookUpTable.add(new LookUpObjects(56,-66.78	,true));
        this.lookUpTable.add(new LookUpObjects(57,-67.05	,true));
        this.lookUpTable.add(new LookUpObjects(58,-67.32	,true));
        this.lookUpTable.add(new LookUpObjects(59,-67.60	,true));
        this.lookUpTable.add(new LookUpObjects(60,-67.89	,true));
        this.lookUpTable.add(new LookUpObjects(61,-68.18	,true));
        this.lookUpTable.add(new LookUpObjects(62,-68.48	,true));
        this.lookUpTable.add(new LookUpObjects(63,-68.79	,true));
        this.lookUpTable.add(new LookUpObjects(64,-69.10	,true));
        this.lookUpTable.add(new LookUpObjects(65,-69.77	,true));
        this.lookUpTable.add(new LookUpObjects(66,-70.69	,true));
        this.lookUpTable.add(new LookUpObjects(67,-71.43	,true));
        this.lookUpTable.add(new LookUpObjects(68,-71.99	,true));
        this.lookUpTable.add(new LookUpObjects(69,-72.38	,true));
        this.lookUpTable.add(new LookUpObjects(70,-72.64	,true));
        this.lookUpTable.add(new LookUpObjects(71,-72.77	,true));
        this.lookUpTable.add(new LookUpObjects(72,-72.79	,true));
        this.lookUpTable.add(new LookUpObjects(73,-72.71	,true));
        this.lookUpTable.add(new LookUpObjects(74,-72.55	,true));
        this.lookUpTable.add(new LookUpObjects(75,-72.33	,true));
        this.lookUpTable.add(new LookUpObjects(76,-72.05	,false));
        this.lookUpTable.add(new LookUpObjects(77,-71.73	,false));
        this.lookUpTable.add(new LookUpObjects(78,-71.39	,false));
        this.lookUpTable.add(new LookUpObjects(79,-71.04	,false));
        this.lookUpTable.add(new LookUpObjects(80,-70.68	,false));
        this.lookUpTable.add(new LookUpObjects(81,-70.32	,false));
        this.lookUpTable.add(new LookUpObjects(82,-69.99	,false));
        this.lookUpTable.add(new LookUpObjects(83,-69.67	,false));
        this.lookUpTable.add(new LookUpObjects(84,-69.39	,false));
        this.lookUpTable.add(new LookUpObjects(85,-69.15	,false));
        this.lookUpTable.add(new LookUpObjects(86,-68.95	,false));
        this.lookUpTable.add(new LookUpObjects(87,-68.80	,false));
        this.lookUpTable.add(new LookUpObjects(88,-68.70	,false));
        this.lookUpTable.add(new LookUpObjects(89,-68.65	,false));
        this.lookUpTable.add(new LookUpObjects(90,-68.66	,false));
        this.lookUpTable.add(new LookUpObjects(91,-68.72	,true));
        this.lookUpTable.add(new LookUpObjects(92,-68.84	,true));
        this.lookUpTable.add(new LookUpObjects(93,-69.01	,true));
        this.lookUpTable.add(new LookUpObjects(94,-69.23	,true));
        this.lookUpTable.add(new LookUpObjects(95,-69.50	,true));
        this.lookUpTable.add(new LookUpObjects(96,-69.81	,true));
        this.lookUpTable.add(new LookUpObjects(97,-70.15	,true));
        this.lookUpTable.add(new LookUpObjects(98,-70.52	,true));
        this.lookUpTable.add(new LookUpObjects(99,-70.91	,true));
        this.lookUpTable.add(new LookUpObjects(100,-71.32	,true));
        this.lookUpTable.add(new LookUpObjects(101,-71.73	,true));
        this.lookUpTable.add(new LookUpObjects(102,-72.13	,true));
        this.lookUpTable.add(new LookUpObjects(103,-72.51	,true));
        this.lookUpTable.add(new LookUpObjects(104,-72.86	,true));
        this.lookUpTable.add(new LookUpObjects(105,-73.16	,false));
        this.lookUpTable.add(new LookUpObjects(106,-73.40	,false));
        this.lookUpTable.add(new LookUpObjects(107,-73.58	,false));
        this.lookUpTable.add(new LookUpObjects(108,-73.67	,false));
        this.lookUpTable.add(new LookUpObjects(109,-73.65	,false));
        this.lookUpTable.add(new LookUpObjects(110,-73.52	,false));
        this.lookUpTable.add(new LookUpObjects(111,-73.25	,false));
        this.lookUpTable.add(new LookUpObjects(112,-72.83	,false));
        this.lookUpTable.add(new LookUpObjects(113,-72.25	,false));
        this.lookUpTable.add(new LookUpObjects(114,-71.48	,false));
        this.lookUpTable.add(new LookUpObjects(115,-70.51	,false));
        this.lookUpTable.add(new LookUpObjects(116,-68.90	,false));
        this.lookUpTable.add(new LookUpObjects(117,-68.66	,false));
        this.lookUpTable.add(new LookUpObjects(118,-68.41	,false));
        this.lookUpTable.add(new LookUpObjects(119,-68.17	,false));
        this.lookUpTable.add(new LookUpObjects(120,-67.94	,false));
        this.lookUpTable.add(new LookUpObjects(121,-67.71	,false));
        this.lookUpTable.add(new LookUpObjects(122,-67.48	,false));
        this.lookUpTable.add(new LookUpObjects(123,-67.26	,false));
        this.lookUpTable.add(new LookUpObjects(124,-67.05	,false));
        this.lookUpTable.add(new LookUpObjects(125,-66.84	,false));
        this.lookUpTable.add(new LookUpObjects(126,-66.63	,false));
        this.lookUpTable.add(new LookUpObjects(127,-66.42	,false));
        this.lookUpTable.add(new LookUpObjects(128,-66.22	,false));
        this.lookUpTable.add(new LookUpObjects(129,-66.03	,false));
        this.lookUpTable.add(new LookUpObjects(130,-65.84	,false));
        this.lookUpTable.add(new LookUpObjects(131,-65.65	,false));
        this.lookUpTable.add(new LookUpObjects(132,-65.47	,false));
        this.lookUpTable.add(new LookUpObjects(133,-65.30	,false));
        this.lookUpTable.add(new LookUpObjects(134,-65.12	,false));
        this.lookUpTable.add(new LookUpObjects(135,-64.96	,false));
        this.lookUpTable.add(new LookUpObjects(136,-64.79	,false));
        this.lookUpTable.add(new LookUpObjects(137,-64.63	,false));
        this.lookUpTable.add(new LookUpObjects(138,-64.48	,false));
        this.lookUpTable.add(new LookUpObjects(139,-64.33	,false));
        this.lookUpTable.add(new LookUpObjects(140,-64.18	,false));
        this.lookUpTable.add(new LookUpObjects(141,-64.04	,false));
        this.lookUpTable.add(new LookUpObjects(142,-63.90	,false));
        this.lookUpTable.add(new LookUpObjects(143,-63.77	,false));
        this.lookUpTable.add(new LookUpObjects(144,-63.64	,false));
        this.lookUpTable.add(new LookUpObjects(145,-63.52	,false));
        this.lookUpTable.add(new LookUpObjects(146,-63.40	,false));
        this.lookUpTable.add(new LookUpObjects(147,-63.28	,false));
        this.lookUpTable.add(new LookUpObjects(148,-63.17	,false));
        this.lookUpTable.add(new LookUpObjects(149,-63.06	,false));
        this.lookUpTable.add(new LookUpObjects(150,-62.96	,false));
        this.lookUpTable.add(new LookUpObjects(151,-62.86	,false));
        this.lookUpTable.add(new LookUpObjects(152,-62.77	,false));
        this.lookUpTable.add(new LookUpObjects(153,-62.68	,false));
        this.lookUpTable.add(new LookUpObjects(154,-62.60	,false));
        this.lookUpTable.add(new LookUpObjects(155,-62.52	,false));
        this.lookUpTable.add(new LookUpObjects(156,-62.44	,false));
        this.lookUpTable.add(new LookUpObjects(157,-62.37	,false));
        this.lookUpTable.add(new LookUpObjects(158,-62.30	,false));
        this.lookUpTable.add(new LookUpObjects(159,-62.24	,false));
        this.lookUpTable.add(new LookUpObjects(160,-62.18	,false));
        this.lookUpTable.add(new LookUpObjects(161,-62.13	,false));
        this.lookUpTable.add(new LookUpObjects(162,-62.08	,false));
        this.lookUpTable.add(new LookUpObjects(163,-62.03	,false));
        this.lookUpTable.add(new LookUpObjects(164,-61.99	,false));
        this.lookUpTable.add(new LookUpObjects(165,-61.96	,false));
        this.lookUpTable.add(new LookUpObjects(166,-61.92	,false));
        this.lookUpTable.add(new LookUpObjects(167,-61.90	,false));
        this.lookUpTable.add(new LookUpObjects(168,-61.87	,false));
        this.lookUpTable.add(new LookUpObjects(169,-61.85	,false));
        this.lookUpTable.add(new LookUpObjects(170,-61.84	,false));
        this.lookUpTable.add(new LookUpObjects(171,-61.83	,false));
        this.lookUpTable.add(new LookUpObjects(172,-61.82	,false));
        this.lookUpTable.add(new LookUpObjects(173,-61.82	,false));
        this.lookUpTable.add(new LookUpObjects(174,-61.83	,false));
        this.lookUpTable.add(new LookUpObjects(175,-61.84	,false));
        this.lookUpTable.add(new LookUpObjects(176,-61.85	,false));
        this.lookUpTable.add(new LookUpObjects(177,-61.86	,false));
        this.lookUpTable.add(new LookUpObjects(178,-61.88	,false));
        this.lookUpTable.add(new LookUpObjects(179,-61.91	,false));
        this.lookUpTable.add(new LookUpObjects( 180,-61.94	,false));



    }

}
