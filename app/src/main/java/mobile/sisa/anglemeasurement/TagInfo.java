package mobile.sisa.anglemeasurement;

public class TagInfo {
    private String tagName;
    private int peakRSSI;
    private short tagPhase;
    private String timeStamp;
    private int angleInfo;

    public TagInfo(String nameIn, int rssiIn, short phaseIn, String timeStampIn, int angleInfoIn) {
        this.tagName=nameIn;
        this.peakRSSI=rssiIn;
        this.tagPhase=phaseIn;
        this.timeStamp=timeStampIn;
        this.angleInfo=angleInfoIn;
    }

    public TagInfo() {

    }

    //Getter
    public String getTagName() {
        return this.tagName;
    }

    public String getPeakRSSI() {
        return ""+this.peakRSSI;
    }

    public int getPeakRSSIValue() {return this.peakRSSI;}

    public String getTagPhase() {
        return ""+this.tagPhase;
    }

    public String getTimeStamp() {
        return this.timeStamp;
    }

    public String getAngleInfo() { return "" + this.angleInfo; }

    //Setter
    public void setTagName (String tagNameIn){this.tagName = tagNameIn;}

    public void setRSSI (String rssiIn) { this.peakRSSI = Integer.parseInt(rssiIn);}

    public void setTagPhase(String phaseIn) {this.tagPhase = Short.parseShort(phaseIn); }

    public void setTimeStamp(String timeStampIn) {this.timeStamp = timeStampIn; }

    public void setAngle(String AngleIn) {this.angleInfo = Integer.parseInt(AngleIn); }
}
