package mobile.sisa.anglemeasurement;

public class LookUpObjects {

    private int iAngle;
    private double fRssi;
    private boolean bPhase;

    public LookUpObjects(int iAngleIn, double fRssiIn, boolean bPhaseIn) {
        this.bPhase=bPhase;
        this.fRssi=fRssiIn;
        this.iAngle=iAngleIn;
    }

    public int getAngle() {
        return this.iAngle;
    }

    public double getRssi() {
        return this.fRssi;
    }

    public boolean getPhasePositive() {
        return this.bPhase;
    }
}
