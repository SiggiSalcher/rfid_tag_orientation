package mobile.sisa.anglemeasurement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class dbDataSource {

    private SQLiteDatabase db;
    private dbHelper dbHelp;

    private String measurementDate;
    private String [] dbColumns = {"ID", "TagName", "RSSI", "Phase", "TimeStamp", "Angle"};

    public dbDataSource (MeasureMent_Main context, String timeStampIn) {

        this.measurementDate = timeStampIn;
        dbHelp = new dbHelper (context, timeStampIn);
    }

    public void dbOpen() throws SQLException {
        db=dbHelp.getWritableDatabase();
        //dbHelp.onCreate(db);

    }

    public void dbClose() {
        dbHelp.close();
    }

    public TagInfo insertValue (String tagNameIn, String rssiIn, String phaseIn, String timeStampIn, String angleValueIn) {
        ContentValues content = new ContentValues();

        content.put("TagName", tagNameIn);
        content.put("RSSI", rssiIn);
        content.put("Phase", phaseIn);
        content.put("TimeStamp", timeStampIn);
        content.put("Angle", angleValueIn);

        dbHelp.checkTable(db);

        long insertID = db.insert(measurementDate, null, content);

        Cursor cursor = db.query(measurementDate, dbColumns, "ID = "+ insertID, null, null, null, null);
        cursor.moveToFirst();

        return cursorToEntry(cursor);

    }

    protected TagInfo cursorToEntry(Cursor cursor) {
        TagInfo runner_time_id_signal = new TagInfo();

        runner_time_id_signal.setTagName(cursor.getString(1));
        runner_time_id_signal.setRSSI(cursor.getString(2));
        runner_time_id_signal.setTagPhase(cursor.getString(3));
        runner_time_id_signal.setTimeStamp(cursor.getString(4));
        runner_time_id_signal.setAngle(cursor.getString(5));

        //kA Warum hier, aber das is halt mal so!

        return runner_time_id_signal;
    }
}
