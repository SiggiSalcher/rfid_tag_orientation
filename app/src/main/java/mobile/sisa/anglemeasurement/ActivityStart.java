package mobile.sisa.anglemeasurement;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.zebra.rfid.api3.DYNAMIC_POWER_OPTIMIZATION;
import com.zebra.rfid.api3.ENUM_TRANSPORT;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.TAG_FIELD;
import com.zebra.rfid.api3.TagData;
import com.zebra.rfid.api3.TagStorageSettings;

import java.util.ArrayList;

public class ActivityStart extends AppCompatActivity {

    private static Readers readers;
    private static ReaderDevice readerDevice;
    public static RFIDReader reader;
    private static ArrayList<ReaderDevice> availableRFIDReaderList;

    private ListView listView;
    private String TagID;

    //ListView Declaration
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> listTagIds = new ArrayList<String>();

    //Eventhandler for RFID-Events
    public static EventHandlerReadingTagsReg eventHandler;

    //Broadcastreceiver for Info-Exchange
    public Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String received;
            listView = (ListView) findViewById(R.id.ListViewTagId);
            //List<String> TagIds = new ArrayList<String>();
            boolean bCheck=true;

            switch (msg.what) {
                case 42:
                    TagID=(String) msg.obj;

                    for(String TagIdCheck:listTagIds) {
                        if (TagID.equals(TagIdCheck))
                        {
                            bCheck=false;
                            break;
                        }
                    }
                    if(bCheck) {
                        listTagIds.add(TagID);
                        arrayAdapter.add(TagID);        //Set new Data and update
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        //Set visibility of ListView to invisible
        listView = (ListView) findViewById(R.id.ListViewTagId);
        listView.setVisibility(View.GONE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            reader.Actions.Inventory.stop();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disconReader();
    }

    public void butStartEvent(View view) throws InvalidUsageException, OperationFailureException {

        AddReader();

        listView = findViewById(R.id.ListViewTagId);
        listView.setVisibility(View.VISIBLE);

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,listTagIds );
        listView.setAdapter(arrayAdapter);

        listView.setClickable(true);

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    reader.Actions.Inventory.stop();
                    reader.Events.removeEventsListener(eventHandler);
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent (ActivityStart.this,MeasureMent_Main.class);
                intent.putExtra("TagID", listTagIds.get(position));

                startActivity(intent);
            }
        });

        //AddReader();
    }

    public void AddReader() {
        if (readers == null) {
            readers = new Readers(this, ENUM_TRANSPORT.BLUETOOTH);
        }

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (readers != null) {
                        if (readers.GetAvailableRFIDReaderList() != null) {
                            availableRFIDReaderList = readers.GetAvailableRFIDReaderList();

                            if (availableRFIDReaderList.size() != 0) {
                                readerDevice = availableRFIDReaderList.get(0);
                                reader = readerDevice.getRFIDReader();

                                if (!reader.isConnected()) {
                                    reader.connect();
                                    ConfigureReader();
                                    reader.Actions.Inventory.perform();
                                }
                            }
                        }
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                    //Log.d(TAG, "OperationFailureException " + e.getVendorMessage());
                }

                return null;
            }
        }.execute();
    }

    private void ConfigureReader() {
        if (reader.isConnected()) {
            try {
                //receive events from reader
                if (eventHandler == null) {
                    eventHandler = new EventHandlerReadingTagsReg(reader);
                }

                reader.Events.addEventsListener(eventHandler);
                reader.Events.setHandheldEvent(true); //Get Handheld events
                reader.Events.setTagReadEvent(true);    //Get Tag reading events
                reader.Events.setAttachTagDataWithReadEvent(true);


                //prepare for TAGInfos
                //stored Settings from Reader
                TagStorageSettings tagStorageSettings = reader.Config.getTagStorageSettings();
                //ew TagStorageSettings = tagStorageSettings;

                TAG_FIELD[] tag_fields = new TAG_FIELD[2];
                tag_fields[0] = TAG_FIELD.PEAK_RSSI;
                tag_fields[1] = TAG_FIELD.PHASE_INFO;

                //tagStorageSettings.setTagFields(TAG_FIELD.ALL_TAG_FIELDS);

                tagStorageSettings.setTagFields(tag_fields);
                reader.Config.setTagStorageSettings(tagStorageSettings);

                reader.Config.setDPOState(DYNAMIC_POWER_OPTIMIZATION.DISABLE);


            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    protected void setDefaultRFIDTrigger() {
        boolean bCheck = true;

        while (bCheck) {
            try {
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, false);
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                bCheck = false;
            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    protected void removeConnectedReader() {
        boolean bCheck = true;

        while (bCheck) {
            try {
                reader.Events.removeEventsListener(eventHandler);
                reader.disconnect();
                readers.Dispose();

                reader = null;
                readers = null;
                bCheck = false;

                if (eventHandler != null) {
                    eventHandler = null;
                }
            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    protected void disconReader() {
        if (reader != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        reader.Actions.Inventory.stop();
                        reader.Events.removeEventsListener(eventHandler);
                    } catch (InvalidUsageException e) {
                        e.printStackTrace();
                    } catch (OperationFailureException e) {
                        e.printStackTrace();
                    }

                    reader.Actions.purgeTags();
                    setDefaultRFIDTrigger();
                    removeConnectedReader();
                    System.gc();

                    return null;
                }
            }.execute();
        }
    }

    public class EventHandlerReadingTagsReg implements RfidEventsListener {

        private RFIDReader reader;

        public EventHandlerReadingTagsReg(RFIDReader readerIn) {
            this.reader = readerIn;
        }

        @SuppressLint("MissingPermission")
        @Override
        public void eventReadNotify(RfidReadEvents rfidReadEvents) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            TagData[] myTags = reader.Actions.getReadTags(10);

            if (myTags != null) {
                for(int index=0; index < myTags.length; index++) {
                    String tagID = myTags[index].getTagID();

                    mHandler.obtainMessage(42, tagID).sendToTarget();
                }
                reader.Actions.purgeTags();
            }
        }

        @Override
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {

        }
    }
}
