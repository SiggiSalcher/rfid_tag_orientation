package mobile.sisa.anglemeasurement;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zebra.rfid.api3.Antennas;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.MEMORY_BANK;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.TagAccess;
import com.zebra.rfid.api3.TagData;
import com.zebra.rfid.api3.TagStorageSettings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MeasureMent_Main extends AppCompatActivity {

    private static Readers readers;
    private static ReaderDevice readerDevice;
    private static RFIDReader reader;
    private static ArrayList<ReaderDevice> availableRFIDReaderList;

    private TextView textViewTagID;
    private TextView textViewAngleInfo;
    private TextView textViewRssi;
    private EditText editTextAngle;

    public int iNumReadings = 100;
    public int iNumRunAvg=10;

    private String TagId;
    private int sAngleValue = 0;

    private readerBackground mReaderTask;

    private dbDataSource database;
    //dbBackground dbBackground;

    private ArrayList<TagInfo> tagList;
    private int[] dMeanRssi = new int[iNumRunAvg];
    private boolean [] bPhasePos = new boolean[iNumRunAvg];



    DateFormat df = new SimpleDateFormat("ddMMyyyy HH:mm:ss");

    private LookUpTable lookUpTable;

    //Database
    private dbDataSource dataSource;

    //Broadcastreceiver for Info-Exchange
    public Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 21: //get Infos from TAG
                    TagInfo received = (TagInfo) msg.obj;

                    double dRssiAverage = fRunAvgRSSI(received.getPeakRSSIValue());
                    boolean bPhaseAverage = fRunAvgPhase(received.getTagPhase());

                    //Working
                    textViewTagID.setText(received.getTagName());
                    textViewAngleInfo.setText(""+lookUpTable.iAngleOut(dRssiAverage,bPhaseAverage));
                    textViewRssi.setText(received.getPeakRSSI());


                    break;

                case 42:
                    fSetVisibilityChangeAngle();
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measure_ment__main);
        Intent intent = getIntent();

        TagId = intent.getStringExtra("TagID");

        fSetVisibilityDefault();

        /*
        //set default values for Application
        LinearLayout TagInfoLayout = findViewById(R.id.linearLayout_TAGInfo);
        LinearLayout AngleInfoLayout = findViewById(R.id.linearLayout_Angle);
        LinearLayout RSSILayout = findViewById(R.id.linearLayout_RSSI);
        LinearLayout AngleInputLayout = findViewById(R.id.lineaLayout_AngleInfo);

        //Buttons
        Button buttonStartStop = findViewById(R.id.button_Stop);
        Button buttonAngleChanged = findViewById(R.id.butChangeAngle);

        //Values
        textViewTagID = findViewById(R.id.tagID_value);
        textViewAngleInfo = findViewById(R.id.angle_value);
        textViewRssi = findViewById(R.id.rssi_value);

        //default hidden proberties
        TagInfoLayout.setVisibility(View.VISIBLE);
        AngleInfoLayout.setVisibility(View.VISIBLE);
        RSSILayout.setVisibility(View.VISIBLE);
        AngleInputLayout.setVisibility(View.INVISIBLE);
        buttonAngleChanged.setVisibility(View.INVISIBLE);

        */

        //Setting running average to 0
        for (int i=0;i<dMeanRssi.length;i++){
            dMeanRssi[i]=0;
            bPhasePos[i]=false;
        }

        lookUpTable = new LookUpTable();

        String timeIn=df.format(Calendar.getInstance().getTime());

        String date = timeIn.substring(0,8);    //Get Corrent Date
        date="'"+date+"'";                      //!!VERY IMPORTANT: Numbers are not accepted -> '' needed!!

        dataSource = new dbDataSource(this, date);

        //getting Reader properties
        reader = ActivityStart.reader;

        mReaderTask=new readerBackground();
        mReaderTask.execute(TagId);
    }

    //Overriding onPause and onResume for save states
    @Override
    protected void onPause() {
        super.onPause();
        disconReader();

        Intent intent = new Intent(this, ActivityStart.class);
        startActivity(intent);
    }

    //Overriding onDestroy function for save state
    @Override
    protected void onDestroy() {
        super.onDestroy();
        disconReader();
    }

    //Event if StopButton is pressed
    public void butStopEvent(View view) throws InvalidUsageException, OperationFailureException {
        //Cancel BackgroundTask
        if(mReaderTask.running) {
            mReaderTask.onCancelled();
        }

        //Disconnect Reader
        disconReader();

        //dbBackground dbBackground = new dbBackground();


        /*Get Data for Database
        String timeIn=df.format(Calendar.getInstance().getTime());

        String date = timeIn.substring(0,8);    //Get Corrent Date
        date="'"+date+"'";                      //!!VERY IMPORTANT: Numbers are not accepted -> '' needed!!
        //String time = timeIn.substring(9,17);   //Get Current Time
        //time="'"+time+"'";

        //Database Entries
        //dataSource = new dbDataSource(this, date);
        //dbBackground.execute();

        //Open Start Activity
        Intent intent = new Intent(this, ActivityStart.class);
        startActivity(intent);*/
    }

    protected void fSetVisibilityChangeAngle() {
        //set default values for Application
        LinearLayout TagInfoLayout = findViewById(R.id.linearLayout_TAGInfo);
        LinearLayout AngleInfoLayout = findViewById(R.id.linearLayout_Angle);
        LinearLayout RSSILayout = findViewById(R.id.linearLayout_RSSI);
        LinearLayout AngleInputLayout = findViewById(R.id.lineaLayout_AngleInfo);

        //Buttons
        Button buttonAngleChanged = findViewById(R.id.butChangeAngle);

        //default hidden proberties
        TagInfoLayout.setVisibility(View.INVISIBLE);
        AngleInfoLayout.setVisibility(View.INVISIBLE);
        RSSILayout.setVisibility(View.INVISIBLE);

        AngleInputLayout.setVisibility(View.VISIBLE);
        buttonAngleChanged.setVisibility(View.VISIBLE);
    }

    protected void fSetVisibilityDefault() {
        //set default values for Application
        LinearLayout TagInfoLayout = findViewById(R.id.linearLayout_TAGInfo);
        LinearLayout AngleInfoLayout = findViewById(R.id.linearLayout_Angle);
        LinearLayout RSSILayout = findViewById(R.id.linearLayout_RSSI);
        LinearLayout AngleInputLayout = findViewById(R.id.lineaLayout_AngleInfo);

        //Buttons
        Button buttonStartStop = findViewById(R.id.button_Stop);
        Button buttonAngleChanged = findViewById(R.id.butChangeAngle);

        //Values
        textViewTagID = findViewById(R.id.tagID_value);
        textViewAngleInfo = findViewById(R.id.angle_value);
        textViewRssi = findViewById(R.id.rssi_value);

        //default hidden proberties
        TagInfoLayout.setVisibility(View.VISIBLE);
        AngleInfoLayout.setVisibility(View.VISIBLE);
        RSSILayout.setVisibility(View.VISIBLE);
        AngleInputLayout.setVisibility(View.INVISIBLE);
        buttonAngleChanged.setVisibility(View.INVISIBLE);

        buttonStartStop.setText("Stop");
    }

    protected void setDefaultRFIDTrigger() {
        boolean bCheck = true;

        while (bCheck) {
            try {
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, false);
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                bCheck = false;
            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    protected void removeConnectedReader() {
        boolean bCheck = true;

        while (bCheck) {
            try {
                reader.disconnect();
                //readers.Dispose();

                reader = null;
                readers=null;
                bCheck = false;

            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    protected void disconReader() {
        if (reader != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    //mAsynchTask.cancel(true);
                    reader.Actions.purgeTags();
                    setDefaultRFIDTrigger();
                    removeConnectedReader();
                    System.gc();

                    return null;
                }
            }.execute();
        }
    }

    protected void getTagInfo(final String TagId) {
        //attributes
        TagAccess tagAccess = new TagAccess();
        TagAccess.ReadAccessParams readAccessParams = tagAccess.new ReadAccessParams();
        TagData readAccessTag = null;

        //Getting time stamp
        String timeStamp = df.format(Calendar.getInstance().getTime()).substring(9,17);

        //read MemoryBank from ReaderDevice lateron it will be shifted
        readAccessParams.setMemoryBank(MEMORY_BANK.MEMORY_BANK_TID);

        try {
            readAccessTag = reader.Actions.TagAccess.readWait(TagId, readAccessParams, null);
            TagInfo tagInfo =  new TagInfo(readAccessTag.getTagID(), readAccessTag.getPeakRSSI(), readAccessTag.getPhase(), timeStamp, sAngleValue);

            storeInDatabase(tagInfo);
            //transmission with handler to update UI
            mHandler.obtainMessage(21, tagInfo).sendToTarget();

        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }

        /*
        TagInfo tagInfo =  new TagInfo(readAccessTag.getTagID(), readAccessTag.getPeakRSSI(), readAccessTag.getPhase(), timeStamp, sAngleValue);

        storeInDatabase(tagInfo);
        //transmission with handler to update UI
        mHandler.obtainMessage(21, tagInfo).sendToTarget();

        /*if(tagList==null) {
            tagList = new ArrayList<TagInfo>();
        }
        tagList.add(tagInfo);*/

    }

    public void butChangeAngle(View view) {

        //sAngleValue = Integer.getInteger((String) textViewAngle.getText());
        editTextAngle=findViewById(R.id.valInputAngle);
        sAngleValue = Integer.parseInt(editTextAngle.getText().toString());

        fSetVisibilityDefault();

        mReaderTask=new readerBackground();
        mReaderTask.execute(TagId);
    }

    public class readerBackground extends AsyncTask<String, Void, Void> {

        private boolean running;

        @Override
        protected Void doInBackground(String... TagID) {
            this.running = true;

            while (running) {
                //Test if working
                for (int i= 0;i<iNumReadings;i++) {
                    getTagInfo(TagId);
                    if(!running)
                    {
                        break;
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    e.printStackTrace();
                    }
                }

                //transmission with handler to update UI
                mHandler.obtainMessage(42).sendToTarget();
                break;
            }
            return null;
        }

        protected void onCancelled() {
            this.running=false;
        }
    }

    void storeInDatabase(TagInfo actualTag) {
        try {
            dataSource.dbOpen();
            //for (TagInfo actualTag : tagList) {
            dataSource.insertValue(actualTag.getTagName(),
                    actualTag.getPeakRSSI(),
                    actualTag.getTagPhase(),
                    actualTag.getTimeStamp(),
                    actualTag.getAngleInfo());
            //}
            dataSource.dbClose();
        } catch (Exception ex) {
            dataSource.dbClose();

        }
    }

    public double fRunAvgRSSI(int iRssiIn) {
        double averageRssi=0;

        for(int i=dMeanRssi.length-1;i>0;i--){
            dMeanRssi[i]=dMeanRssi[i-1];
            averageRssi+=dMeanRssi[i];
        }
        dMeanRssi[0]=iRssiIn;
        averageRssi+=dMeanRssi[0];
        averageRssi/=(dMeanRssi.length);

        return averageRssi;
    }

    public boolean fRunAvgPhase(String bPhaseIn) {
        double dPhaseIn = Double.valueOf(bPhaseIn);
        int iNumPos=0;

        for(int i=bPhasePos.length-1;i>0;i--){
            bPhasePos[i]=bPhasePos[i-1];
            if(bPhasePos[i]){
                iNumPos++;
            }
        }
        bPhasePos[0]=dPhaseIn>0; //check if ok else
        /*
        if(dPhaseIn>0){
            bPhasePos[0]=true;
        }
        else {
            bPhasePos[0]=false;
        }
        bPhasePos[0]=averagePhase;
        */
        if(bPhasePos[0]){
            iNumPos++;
        }

        return dPhaseIn>0;
        //return iNumPos>=(bPhasePos.length/2);
    }
}