package mobile.sisa.anglemeasurement;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

    public class dbHelper extends SQLiteOpenHelper {

        private static final String DB_NAME = "PhaseMeasurement.db";
        private static final int DB_VERSION = 1;
        private static final String TBL_COLUMN_ID = "ID";
        private static final String TBL_COLUMN_TAGNAME = "TagName";
        private static final String TBL_COLUMN_RSSI = "RSSI";
        private static final String TBL_COLUMN_PHASE = "Phase";
        private static final String TBL_COLUMN_TIME = "TimeStamp";
        private static final String TBL_COLUMN_ANGLE = "Angle";

        private String TBL_NAME = "Default";

        private String SQL_CREATE;

        private SQLiteDatabase db;

        public dbHelper(Context context, String MeasurementIn) {
            super(context, DB_NAME, null, DB_VERSION);
            this.TBL_NAME = MeasurementIn;
            //db.execSQL(commandCheckNCreateDb());
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(commandCreateDb());
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + this.TBL_NAME);
            onCreate(db);
        }

        public void checkTable(SQLiteDatabase db) {

            db.execSQL(commandCheckNCreateDb());
            //onCreate(db);
        }

        private String commandCreateDb() {
            SQL_CREATE= "CREATE TABLE " + TBL_NAME;
            SQL_CREATE += " (" + TBL_COLUMN_ID;
            SQL_CREATE += " INTEGER PRIMARY KEY AUTOINCREMENT, ";
            SQL_CREATE += TBL_COLUMN_TAGNAME + " TEXT, ";
            SQL_CREATE += TBL_COLUMN_RSSI + " TEXT, ";
            SQL_CREATE += TBL_COLUMN_PHASE + " TEXT, ";
            SQL_CREATE += TBL_COLUMN_TIME + " TEXT, ";
            SQL_CREATE += TBL_COLUMN_ANGLE + " TEXT)";

            return SQL_CREATE;
        }

        private String commandCheckNCreateDb() {
            SQL_CREATE= "CREATE TABLE IF NOT EXISTS " + TBL_NAME;
            SQL_CREATE += " (" + TBL_COLUMN_ID;
            SQL_CREATE += " INTEGER PRIMARY KEY AUTOINCREMENT, ";
            SQL_CREATE += TBL_COLUMN_TAGNAME + " TEXT, ";
            SQL_CREATE += TBL_COLUMN_RSSI + " TEXT, ";
            SQL_CREATE += TBL_COLUMN_PHASE + " TEXT, ";
            SQL_CREATE += TBL_COLUMN_TIME + " TEXT, ";
            SQL_CREATE += TBL_COLUMN_ANGLE + " TEXT)";

            return SQL_CREATE;
        }
        /*
    public void insertIntoDatabase(String rssiIn, String phaseIn) {
        String SQLCommand;

        SQLCommand = "INSERT INTO "+TBL_NAME;
        SQLCommand += " (" + TBL_COLUMN_RSSI ;
        SQLCommand += ", " + TBL_COLUMN_PHASE;
        SQLCommand += " , " + TBL_COLUMN_TIME;
        SQLCommand += ") VALUES ";
        SQLCommand += " ("+ rssiIn;
        SQLCommand += ", " + phaseIn;
        SQLCommand += " , NULL)";
        //SQLCommand += ")";

        executeCommand(SQLCommand);
    }

    private void executeCommand(String SQLCommand){
        db.execSQL(SQLCommand);
    }
    */
}


